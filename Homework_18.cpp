﻿#include <iostream>
using namespace std;
template <class AnyType>

class Stack
{
private:
	AnyType* StackArray = NULL;
	int size = 0;

public:
	Stack() {}
	~Stack() { delete[] StackArray; }

	void Push(AnyType value)
	{
		AnyType* NewArray = new AnyType[size + 1];
		cout << "Добавить элемент " << value << endl;

		for (int i = 0; i < size; i++) { NewArray[i] = StackArray[i]; }
		NewArray[size] = value;
		delete[] StackArray;
		size++;
		StackArray = NewArray;
	}

	void Pop()
	{
		cout << "Удаляем последний элемент " << endl;
		--size;
	}

	void Print()
	{
		for (int i = 0; i < size; i++) { cout << *(StackArray + i) << endl; }
		cout << endl;
	}
};

int main()
{
	setlocale(LC_ALL, "Russian");

	Stack<int> IntStack;

	IntStack.Push(1);
	IntStack.Push(20);
	IntStack.Push(300);
	IntStack.Push(4000);
	IntStack.Push(50000);

	IntStack.Print();
	IntStack.Pop();
	IntStack.Print();
	IntStack.Pop();
	IntStack.Print();
	IntStack.Pop();
	IntStack.Print();
	IntStack.Push(777);
	IntStack.Print();
	IntStack.Push(666);
	IntStack.Print();
	IntStack.Push(555);
	IntStack.Print();
}